# Frogger Arcade Game

Welcome to the classic Frogger arcade game! This game, implemented in TypeScript, allows players to control a frog as it navigates through various obstacles to reach its goal. Below is an overview of the game implementation along with instructions on how to play.

![Screen Shot 2024-02-11 at 21 30 49](https://github.com/AnushkaReddy-hub/TheClassicFroggerArcadeGame/assets/77491598/bd610da8-de49-453d-af07-9b287fd61b71)

## How to Play

1. **Objective**: The objective of the game is to safely guide the frog to the target line at the top of the screen without colliding with any obstacles.

2. **Controls**: Use the arrow keys (ArrowLeft, ArrowRight, ArrowUp, ArrowDown) to move the frog left, right, up, or down respectively.

3. **Gameplay**: The game consists of several lanes where different types of obstacles move. The frog starts at the bottom of the screen in a safe area. The player must navigate the frog across the lanes, avoiding collisions with moving obstacles such as logs, turtles, and vehicles.

4. **Scoring**: Each successful crossing of the river awards the player with points. The player earns more points for reaching the target line quickly and without colliding with obstacles.

5. **Game Over**: The game ends if the frog collides with an obstacle or fails to reach the target line within the time limit.

## Implementation Details

- **Dependencies**: The game utilizes RxJS library for handling asynchronous events such as keyboard input and game ticks.

- **Game Elements**: The game consists of various elements including the frog, logs, turtles, and vehicles. These elements move across the screen according to predefined patterns.

- **Collision Detection**: The game includes collision detection logic to check if the frog collides with any obstacles. If a collision occurs, the game ends, and the player receives a game-over message.

## Running the Game

To run the Frogger arcade game:

1. Build the code and then open the web page

- Run `npm install`
- Run `npm run build`
- Open the html file dist/index.html (NOT src/index.html)

or

2. Use the development server

- Run `npm install`
- Run `npm run dev`, this will automatically open a web page at localhost:4000
- Open localhost:4000 in your browser if it hasn't already

The development server will have some additional features that help with the development process but are not essential.

## Credits

This Frogger arcade game is inspired by the classic arcade game of the same name. It is implemented by Anushka Reddy.

## Have Fun!

Enjoy playing the Frogger arcade game and challenge yourself to beat your high score! If you encounter any issues or have suggestions for improvement, feel free to contribute to the game repository or reach out to the developer
Name: Anushka Reddy
Contact: anushkar614@gmail.com

Happy hopping! 🐸🎮

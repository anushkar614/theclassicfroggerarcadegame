import "./style.css";
import { fromEvent, interval, merge } from 'rxjs';
import { map, filter, scan } from 'rxjs/operators';

type Key = 'ArrowLeft' | 'ArrowRight' | 'ArrowUp' | 'ArrowDown';
type Event = 'keydown' | 'keyup';
type Direction = 'Left-Right' | 'up-Down';
type TurtleOrEnemy = 'Turtle' | 'Log' | 'Enemy';
const grid = 75;  //tile size
const RowCount = 9;
const ColCount = 12;

const initGround = () => {
  const svg = document.getElementById('svgCanvas')!;
  svg.setAttribute('width', (grid * ColCount).toString());
  svg.setAttribute('height', (grid * RowCount).toString());

  // river area
  const riverArea = document.createElementNS(svg.namespaceURI, 'rect')!;
  riverArea.setAttribute('x', '0');
  riverArea.setAttribute('y', '0');
  riverArea.setAttribute('style', 'fill:DarkCyan;');
  riverArea.setAttribute('width', (grid * ColCount).toString());
  riverArea.setAttribute('height', (grid * 4).toString());
  svg.appendChild(riverArea);

  // start area
  const startArea = document.createElementNS(svg.namespaceURI, 'rect')!;
  startArea.setAttribute('x', '0');
  startArea.setAttribute('y', (grid * RowCount - grid).toString());
  startArea.setAttribute('style', 'fill:DarkOliveGreen;');
  startArea.setAttribute('width', (grid * ColCount).toString());
  startArea.setAttribute('height', (grid).toString());
  svg.appendChild(startArea);

  // safe area
  const safeArea = document.createElementNS(svg.namespaceURI, 'rect')!;
  safeArea.setAttribute('x', '0');
  safeArea.setAttribute('y', (grid * (RowCount - 1) / 2).toString());
  safeArea.setAttribute('style', 'fill:DarkOliveGreen;');
  safeArea.setAttribute('width', (grid * ColCount).toString());
  safeArea.setAttribute('height', (grid).toString());
  svg.appendChild(safeArea);

  // target line
  const targetArea = document.createElementNS(svg.namespaceURI, 'rect')!;
  targetArea.setAttribute('x', '0');
  targetArea.setAttribute('y', '0');
  targetArea.setAttribute('style', 'fill:DarkGoldenRod;');
  targetArea.setAttribute('width', (grid * ColCount).toString());
  targetArea.setAttribute('height', (grid).toString());
  svg.appendChild(targetArea);
}

//initalize frog on start point
const createFrog = () => {
  // create frog
  const svg = document.getElementById('svgCanvas')!;
  const g = document.createElementNS(svg.namespaceURI, 'g');
  g.setAttribute('id', 'frog');
  ColCount / 2
  g.setAttribute('transform', `translate(${grid * ColCount / 2 + grid / 2}, ${(grid * RowCount) - (grid / 2)})`);
  const p = document.createElementNS(svg.namespaceURI, 'polygon');
  let w = grid / 2 - 2;
  p.setAttribute('points', `${0 - w}, ${w} ${w}, ${w} 0, ${0 - w}`);
  p.setAttribute('style', 'fill:lime');
  g.appendChild(p);
  svg.appendChild(g);
}

// game manage function
function main() {
  initGround();

  // game common constants
  const Constants = {
    CanvasSizeX: grid * ColCount,
    CanvasSizeY: grid * RowCount,
    FrogSize: grid,
    MoveSpeed: grid,
    StartTime: 0,
    StartPointX: grid * ColCount / 2 + grid / 2,
    StartPointY: (grid * RowCount) - (grid / 2)
  }

  // game state
  type State = Readonly<{
    x: number;  //frog x pos
    y: number;  //frog y pos
    angle: number;  //frog direction
    rows: Array<Array<Enemy>>,  //objects on gameboard
    gameOver: boolean,  //gameover status
    score: 0, //game score
  }>;

  // game obstacle pattern type
  type pattern = Readonly<{
    spacing: Array<number>;
    color: string;
    size: number;
    shape: string;
    count: number;
    type: string;
    speed: number;
  }>;

  // game obstacle type enemy or turtle
  type Enemy = Readonly<{
    objectId: string;
    x: number;
    y: number;
    width: number;
    speed: number;
    color: string;
    type: string;
    shape: string;
  }>;

  // obstacle appearance data
  const patterns = [
    // end bank is safe
    {
      spacing: [1.5, 1.5, 1.5, 1.5, 1.5],      // how many grid spaces between each obstacle
      color: 'DarkRed',  // color of the obstacle
      size: 2,    // width (rect) / diameter (circle) of the obstacle
      shape: 'rect',
      type: 'Log',
      speed: 0        // how fast the obstacle moves and which direction
    },
    // log
    {
      spacing: [0, 2, 0, 2, 0, 2, 0, 2, 0, 2],      // how many grid spaces between each obstacle
      color: 'PaleVioletRed',  // color of the obstacle
      size: 2,    // width (rect) / diameter (circle) of the obstacle
      shape: 'rect',
      type: 'Log',
      speed: -12        // how fast the obstacle moves and which direction
    },
    // turtle
    {
      spacing: [0, 3, 0, 2, 0, 3],
      color: 'RebeccaPurple',
      size: 3,
      shape: 'circle',
      type: 'Turtle',
      speed: -6
    },
    // log
    {
      spacing: [0, 2, 0, 3, 0, 4, 0, 2],      // how many grid spaces between each obstacle
      color: 'Tan',  // color of the obstacle
      size: 3,    // width (rect) / diameter (circle) of the obstacle
      shape: 'rect',
      type: 'Log',
      speed: 10        // how fast the obstacle moves and which direction
    },
    null, //safe area
    // truck
    {
      spacing: [0, 4, 0, 3, 0, 2],
      color: 'SaddleBrown',
      size: 3,
      shape: 'rect',
      type: 'Enemy',
      speed: -8
    },
    // fast car
    {
      spacing: [0, 8, 0, 10],
      color: 'GoldenRod',
      size: 1,
      shape: 'rect',
      type: 'Enemy',
      speed: 12
    },
    // bulldozer
    {
      spacing: [0, 8, 0, 10],
      color: 'DimGray',
      size: 1,
      shape: 'rect',
      type: 'Enemy',
      speed: -8
    },
    null
  ];

  // game state transitions
  class Tick {
    constructor(public readonly elapsed: number) { }
  }
  class MoveLeftRight {
    constructor(public readonly direction: number) { }
  }
  class MoveUpDown {
    constructor(public readonly direction: number) { }
  }
  //initialize obstacle
  const initEnemy = () => {
    let enemyArray: Array<Array<Enemy>> = [];
    patterns.map((pattern, index) => {
      if (!pattern) {
        enemyArray.push([])
      } else {
        let tempArray: Array<Enemy> = [];
        let maxX = 0;
        for (let i = 0; i < pattern.spacing.length; i++) {
          if (pattern.spacing[i] === 0) {
            continue;
          } else {
            if (pattern.shape === 'circle') {
              maxX = maxX + pattern.spacing[i] * grid;
              for (let j = 0; j < pattern.size; j++) {
                tempArray.push(
                  <Enemy>{
                    objectId: pattern.type + '_' + index + '_' + i + '_' + j,
                    x: maxX + grid,
                    y: grid * index + (pattern.shape === 'circle' ? grid / 2 : 0),
                    width: grid,
                    speed: pattern.speed,
                    color: pattern.color,
                    type: pattern.type,
                    shape: pattern.shape,
                  })
                maxX = maxX + grid;
              }
            } else {
              tempArray.push(
                <Enemy>{
                  objectId: pattern.type + '_' + index + '_' + i,
                  x: maxX + pattern.spacing[i] * grid,
                  y: grid * index,
                  width: pattern.size * grid,
                  speed: pattern.speed,
                  color: pattern.color,
                  type: pattern.type,
                  shape: pattern.shape,
                })
              maxX = maxX + pattern.spacing[i] * grid + pattern.size * grid;
            }
          }
        }
        enemyArray.push(tempArray);
      }
    })
    return enemyArray;
  }

  //initialize game state
  const initialState: State = { x: Constants.StartPointX, y: Constants.StartPointY, angle: 0, rows: initEnemy(), gameOver: false, score: 0 };

  // add all obstacles on svg
  const showEnemies = () => {
    const svg = document.getElementById('svgCanvas')!;
    initialState.rows.map(row => {
      row.map(item => {
        if (item.shape === 'circle') {
          const v = document.createElementNS(svg.namespaceURI, 'ellipse')!;
          v.setAttribute('id', item.objectId);
          v.setAttribute('rx', (grid / 2).toString());
          v.setAttribute('ry', (grid / 2).toString());
          v.setAttribute('cx', item.x.toString());
          v.setAttribute('cy', item.y.toString());
          v.setAttribute('style', `fill:${item.color};`);
          svg.appendChild(v);
        } else {
          const v = document.createElementNS(svg.namespaceURI, 'rect')!;
          v.setAttribute('id', item.objectId);
          v.setAttribute('x', item.x.toString());
          v.setAttribute('y', item.y.toString());
          v.setAttribute('width', item.width.toString());
          v.setAttribute('height', grid.toString());
          v.setAttribute('style', `fill:${item.color};`);
          svg.appendChild(v);
        }
      })
    })
  }

  // move obstacles
  const moveEnemy = (item: Enemy, index: number) => {
    const v = document.getElementById(item.objectId)!;
    if (item.shape === 'circle') {
      let cx = Number.parseFloat(v.getAttribute('cx')?.toString()!);
      let x = cx + item.speed;
      let maxSize = 0;
      patterns[index]?.spacing.map(t => maxSize = maxSize + patterns[index]?.size! * grid * t);
      if (x > (maxSize + grid / 2)) {
        x = 0 - grid / 2;
      }
      if (x < (0 - grid / 2)) {
        x = (maxSize + grid / 2);
      }
      v.setAttribute('cx', x.toString());
    }
    if (item.shape === 'rect') {
      let cx = Number.parseFloat(v.getAttribute('x')?.toString()!);
      let x = cx + item.speed;
      let maxSize = 0;
      patterns[index]?.spacing.map(t => maxSize = maxSize + Number.parseInt(v.getAttribute('width')?.toString()!) * t);
      if (x > maxSize - Number.parseInt(v.getAttribute('width')?.toString()!)) {
        x = (0 - item.width);
      }
      if (x < 0 - Number.parseInt(v.getAttribute('width')?.toString()!)) {
        x = maxSize - Number.parseInt(v.getAttribute('width')?.toString()!);
      }
      v.setAttribute('x', x.toString());
    }

  }

  showEnemies();
  createFrog();

  //event observable
  const gameClock = interval(100).pipe(map((elapsed) => new Tick(elapsed))),
    keyObservable = <T>(e: Event, k: Key, result: () => T) =>
      fromEvent<KeyboardEvent>(document, e).pipe(
        filter(({ code }) => code === k),
        map(result)
      ),
    moveLeft = keyObservable('keydown', 'ArrowLeft', () => new MoveLeftRight(-1)),
    moveRight = keyObservable('keydown', 'ArrowRight', () => new MoveLeftRight(1)),
    moveForward = keyObservable('keydown', 'ArrowUp', () => new MoveUpDown(-1)),
    moveBackward = keyObservable('keydown', 'ArrowDown', () => new MoveUpDown(1));

  //detect frog collision
  const handleCollision = (s: State) => {
    let froggerRow = Math.floor(s.y / grid);
    let collied: boolean = false;
    let gameOver = false;
    s.rows[froggerRow].map(item => {
      let itemX = 0;
      const itemTag = document.getElementById(item.objectId);
      if (item.shape == 'rect') {
        itemX = Number.parseFloat(itemTag?.getAttribute('x')?.toString()!);
      } else {
        itemX = Number.parseFloat(itemTag?.getAttribute('cx')?.toString()!);
      }

      if (itemX > (s.x - grid / 2 - item.width) && itemX < (s.x + grid / 2)) {
        collied = true;
        gameOver = true;
      }
    })
    if (froggerRow < 4) {
      if (collied) {
        gameOver = false;
        if (froggerRow === 0) {
          return <State>{
            ...s,
            score: s.score + 1,
            x: Constants.StartPointX,
            y: Constants.StartPointY
          }
        } else {
          return <State>{
            ...s,
            x: (s.x - grid / 2) > 0 && (s.x + grid / 2) < Constants.CanvasSizeX ? s.x + s.rows[froggerRow][0].speed : s.x
          }
        }
      } else {
        gameOver = true;
      }
    }
    return <State>{
      ...s,
      gameOver: gameOver,
    }
  }
  const tick = (s: State, elapsed: number) => {
    initialState.rows.map((row, index) => {
      row.map(item => {
        moveEnemy(item, index);
      })
    });
    if (s.gameOver) {
      return <State>{
        ...s,
        gameOver: false,
        x: Constants.StartPointX,
        y: Constants.StartPointY,
        angle: 0
      }

    }
    return handleCollision(s);
  };
  const move = (s: State, direction: number, type: Direction) => {
    let x = s.x;
    let y = s.y;
    let angle = s.angle;
    if (type === 'Left-Right') {
      x = x + direction * Constants.MoveSpeed;
      angle = direction === 1 ? 90 : 270;
    }
    if (type === 'up-Down') {
      y = y + direction * Constants.MoveSpeed;
      angle = direction === 1 ? 180 : 0;
    }
    if (x > Constants.CanvasSizeX) {
      x = x - Constants.MoveSpeed;
    }
    if (x < 0) {
      x = x + Constants.MoveSpeed;
    }
    if (y > Constants.CanvasSizeY) {
      y = y - Constants.MoveSpeed;
    }
    if (y < 0) {
      y = y + Constants.MoveSpeed;
    }
    return {
      ...s,
      x: x,
      y: y,
      angle: angle
    }
  }

  // state transducer
  const reduceState = (s: State, e: MoveLeftRight | MoveUpDown | Tick) =>
    e instanceof MoveLeftRight ? move(s, e.direction, 'Left-Right') :
      e instanceof MoveUpDown ? move(s, e.direction, 'up-Down') :
        tick(s, e.elapsed);
  function updateView(s: State) {
    const frog = document.getElementById("frog");
    frog?.setAttribute('transform', `translate(${s.x}, ${s.y}) rotate(${s.angle})`)
    const svg = document.getElementById('svgCanvas')!;
    if (s.gameOver) {
      subscription.unsubscribe();
      const v = document.createElementNS(svg.namespaceURI, 'text');
      v.setAttribute('x', (Constants.CanvasSizeX / 4).toString());
      v.setAttribute('y', (Constants.CanvasSizeY / 2).toString());
      v.setAttribute('class', 'gameOver');
      v.textContent = 'Game Over';
      svg.appendChild(v);
    }
    const score = document.getElementById('score')!;
    score.innerHTML = s.score.toString()
  }

  const
    subscription = merge(
      gameClock,
      moveLeft,
      moveRight,
      moveForward,
      moveBackward,
      
    ).pipe(scan(reduceState, initialState))
      .subscribe(updateView);
}

// The following simply runs your main function on window load.  Make sure to leave it in place.
if (typeof window !== "undefined") {
  window.onload = () => {
    main();
  };
}
